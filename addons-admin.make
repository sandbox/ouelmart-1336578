;******************************************************************************
;                                 Documentation
;******************************************************************************
;
; Description:
; A drush makefile to download some admin helper modules

;******************************************************************************
;                                    General
;******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 6.x

;******************************************************************************
;                              Admin helper modules
;******************************************************************************

projects[admin_menu][destination] = contrib
projects[openidadmin][destination] = contrib
projects[masquerade][destination] = contrib
projects[journal][destination] = contrib
projects[views_bulk_operations][destination] = contrib
projects[taxonomy_manager][destination] = contrib
projects[stringoverrides][destination] = contrib
projects[admin_theme][destination] = contrib
projects[util][destination] = contrib
projects[filter_perms][destination] = contrib

; Themes
;projects[root_candy][destination] = contrib
projects[rubik][destination] = contrib

;******************************************************************************
;                                      End
;******************************************************************************
