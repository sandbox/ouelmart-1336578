;******************************************************************************
;                                Documentation
;******************************************************************************

; Description:
; A stub drush makefile to create an up-to-date OpenAtrium installation profile
; on a Pressflow core.

; Instructions:
; In your platforms directory (/var/aegir/platforms), run:
;   "drush make openatrium-stub.make"
; or create a new platform by calling this makefile:
;   drush make ../makefiles/stub-communautes-durables.make -y --contrib-destination=profiles/openatrium --force-complete --working-copy <nom>
; using Aegir's web interface (node/add/platform).

;******************************************************************************
;                                   General
;******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 6.x

;******************************************************************************
;                               Include files
;******************************************************************************

; get fast core and set version
; includes[pressflow] = "core-pressflow-6.make"
; projects[pressflow][download][tag] = DRUPAL-6-22
projects[drupal][type] = core

includes[openatrium] = "profile-openatrium.make"
projects[openatrium][download][tag] = 6.x-1.0

;includes[drupal_modules_maj] = includes/drupal-6-modules-maj.make

;******************************************************************************
;                        Additional features (optional)
;******************************************************************************


;;;;soder/contrib
projects[admin_menu][subdir] = "soder/contrib"

; add patch to feeds_tamper
projects[feeds_tamper][type] = "module"
projects[feeds_tamper][subdir] = "soder/contrib"
projects[feeds_tamper][version] = "1.x-dev"
;projects[feeds_tamper][patch][] = ""



;;;;soder/custom 
; projects[action_select][type] = "module"
; projects[action_select][subdir] = "soder/custom"
; projects[action_select][download][type] = "git"
; projects[action_select][download][url] = "http://git.drupal.org/sandbox/ouelmart/1300306.git"

projects[ct_feeds][subdir] = "soder/custom"
projects[ct_feeds][download][type] = "git"
projects[ct_feeds][download][url] = "http://git.drupal.org/project/ct_feeds.git"
projects[ct_feeds][download][branch] = "6.x-1.x"
; for error related to uuid, ref: http://drupal.org/node/1065364 & http://drupal.org/node/895622

; projects[soder_features][type] = "module"
; projects[soder_features][subdir] = "soder/custom/features"
; projects[soder_features][download][type] = "git"
; projects[soder_features][download][url] = "http://git.drupal.org/sandbox/ouelmart/1336616.git"

; projects[dossier_cpe][type] = "module"
; projects[dossier_cpe][subdir] = "soder/custom"
; projects[dossier_cpe][download][type] = "git"
; projects[dossier_cpe][download][url] = "http://git.drupal.org/sandbox/ouelmart/1372136.git"

; projects[soder_cron][type] = "module"
; projects[soder_cron][subdir] = "soder/custom"
; projects[soder_cron][download][type] = "git"
; projects[soder_cron][download][url] = "http://git.drupal.org/sandbox/ouelmart/1407164.git"

; projects[link_eg][type] = "module"
; projects[link_eg][subdir] = "soder/custom"
; projects[link_eg][download][type] = "git"
; projects[link_eg][download][url] = "http://git.drupal.org/sandbox/ouelmart/1491028.git"

; projects[action_select_feature][type] = "module"
; projects[action_select_feature][subdir] = "soder/custom"
; projects[action_select_feature][download][type] = "git"
; projects[action_select_feature][download][url] = "http://git.drupal.org/sandbox/ouelmart/1492520.git"



;;;;contrib
projects[feeds][subdir] = "contrib"
; projects[feeds][download][url] = "git://drupalcode.org/project/feeds.git"
projects[feeds][version] = "1.x-dev"
; projects[feeds][patch][] = "http://drupal.org/files/issues/724536-128-feeds-nodereference-mapper.patch"
; projects[feeds][patch][] = "http://drupal.org/files/issues/add_new_hook-1288814-1.patch"

projects[login_destination][subdir] = "contrib"

projects[filefield][subdir] = "contrib"

projects[imagefield][subdir] = "contrib"

projects[views_embed_form][subdir] = "contrib"

projects[editablefields][subdir] = "contrib" 

projects[ajax_load][subdir] = "contrib" 








; projects[soder_atrium_casetracker][type] = "module"
; projects[soder_atrium_casetracker][subdir] = "soder/custom/features"
; projects[soder_atrium_casetracker][download][type] = "git"
; projects[soder_atrium_casetracker][download][url] = "http://git.drupal.org/sandbox/ouelmart/1336904.git"

;;;;add patch to feeds_xpathparser
; projects[feeds_xpathparser][patch][] = "http://drupal.org/files/issues/FeedsXPathParser-1156062-6.patch"


;;;; Features Utilities ;;;;

; features_plumber
; ftools
; features_override


; Atrium Spreadsheets
; Atrium Bookmarks
; http://drupal.org/project/atrium_glossary
; http://drupal.org/project/atrium_leads
; https://github.com/nuvoleweb/atrium_folders
; http://drupal.org/project/oa_iterations
; http://drupal.org/sandbox/phishsauce/1219452
; http://drupal.org/project/meetings
; http://drupal.org/project/ideation
; http://drupal.org/project/casetracker_comment_driven


;******************************************************************************
;                                     End
;******************************************************************************
