;****************************************
; Documentation
;****************************************

; Description:
; A drush makefile for OpenAtrium profile.

;****************************************
; General
;****************************************

; drush make API version
api = 2

; Drupal core
core = 6.x

;****************************************
; OpenAtrium profile
;****************************************

projects[openatrium][type] = profile
projects[openatrium][download][type] = git
projects[openatrium][download][url] = git://drupalcode.org/project/openatrium.git

;****************************************
; Modules to overwrite version
;****************************************

;projects[devel][destination] = profiles/openatrium/modules/developer
;projects[devel][version] = 1.25

;****************************************
; End
;****************************************
